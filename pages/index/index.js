//获取应用实例
var app = getApp();


var config = {
    data: {
        disable: false,
        gameList: ['4x4','5x5']
    },

    onLoad: function() {
        var that = this
            //调用应用实例的方法获取全局数据
        app.getUserInfo(function(userInfo) {
            //更新数据
            that.setData({
                userInfo: userInfo
            })
        })
    },
    onReady: function() {
        // 页面渲染完毕
    },
    onShow: function() {
        // 页面展示
    },
    onHide: function() {
        // 页面隐藏
    },
    onUnload: function() {
        // 页面关闭
    },
    //事件处理函数
    ToRankPage: function () {
      wx.navigateTo({
        url: '../rank/rank'
      })
    },
    ToHelpPage: function () {
      wx.navigateTo({
        url: '../help/help'
      })
    },
    To4x4Page: function () {
      wx.navigateTo({
        url: '../4x4/4x4'
      })
    },
    To5x5Page: function () {
      wx.navigateTo({
        url: '../5x5/5x5'
      })
    },
    /**
 * 用户点击右上角分享
 */
    onShareAppMessage: function () {
      return {
        title: '2048精版',
        path: '/pages/index/index',
        // imageUrl: this.currentCanvasImg,
      }
    }
};

//设置属性名"startName"到相应页面的映射
config.data.gameList.forEach(function(v) {
    config['start' + v] = function() {

        config.data.disable = true;

        // 这里需要注意每个文件夹名称需和js名称保持一致
        wx.navigateTo({
            url: '../' + v + '/' + v
        })
    }
});

Page(config);
